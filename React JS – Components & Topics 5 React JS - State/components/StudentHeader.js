import React from "react";
class StudentHeader extends React.Component {
    render() {
        return (
            <React.Fragment>
                {this.props.headers.map((key, index) => {
                    return <th key={index}>{key.toUpperCase()}</th>
                })
                }
            </React.Fragment>
        );
    }
}

export default StudentHeader;