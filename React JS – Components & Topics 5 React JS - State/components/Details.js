import React from "react";
class Details extends React.Component {

  render() {
    return (
      <React.Fragment>
        {this.props.students.map((student, index) => {
          const { studentId, studentName, studentMarks } = student //destructuring
          return (<tr key={studentId + index}>
            <td>{studentId}</td>
            <td>{studentName}</td>
            <td>{studentMarks}</td>
          </tr>
          )
        })}
      </React.Fragment>
    );
  }
}

export default Details;