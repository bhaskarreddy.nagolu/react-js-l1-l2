import React from "react";
import Header from './Header';
import StudentHeader from './StudentHeader';
import Details from './Details';
import students from './students.json';
import './student-table.css';
class Main extends React.Component {
    constructor() {
        super();
        this.state = {
            students: students
        };
    }
    render() {
        return (
            <React.Fragment>
                <Header></Header>
                <table className="students">
                    <tbody>
                        <tr>
                            <StudentHeader headers={Object.keys(this.state.students[0])}></StudentHeader>
                        </tr>
                        <Details students={this.state.students}></Details>
                    </tbody>
                </table>

            </React.Fragment>
        );
    }
}

export default Main;