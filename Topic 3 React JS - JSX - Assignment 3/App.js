import React from "react";
import './multiplication-table.css';
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      value: '', table: null,
      maxTable: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    };
    this.onChange = this.onChange.bind(this);
  }


  onChange = (event) => {
    const re = /^[0-9\b]+$/;
    if (event.target.value === '' || re.test(event.target.value)) {
      this.setState({ value: event.target.value })
    }
  }

  renderTableData = () => {
    let tableData = <table className="multiplication-table">
      <tbody>
        <tr></tr>
        {this.state.maxTable.map((number, index) => {
          return (
            <tr key={number + index}>
              <td>{this.state.value + ' * ' + number}</td>
              <td>{' = '}</td>
              <td>{number * this.state.value}</td>
            </tr>
          )
        })}
      </tbody>
    </table>

    this.setState({ table: tableData })
  }

  render() {
    return (
      <React.Fragment>
        <h3>Table Multiplication</h3>
        Table for Multiplication  :
        &nbsp;<input value={this.state.value} onChange={this.onChange}></input>
        &nbsp; <input type="button" value="Generate Table" onClick={this.renderTableData}></input>
        {this.state.table}
      </React.Fragment>
    );
  }
}

export default App;