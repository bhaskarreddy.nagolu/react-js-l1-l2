import React from "react";
import './multiplication-table.css';
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      value: 5,
      table: [],
      initial: 0
    };
  }

  renderTableData = () => {
    const number = this.state.initial + 1;
    let tableData =
      <tr key={number}>
        <td>{this.state.value + ' * ' + number}</td>
        <td>{' = '}</td>
        <td>{number * this.state.value}</td>
      </tr>
    this.setState((prevState) => ({ table: prevState.table.concat(tableData), initial: number }))
  }

  render() {
    return (
      <React.Fragment>
        <h3>Table Multiplication</h3>
        <input type="button" value="Click to generate Multiplication tables of 5" onClick={this.renderTableData}></input>
        <br />
        <br />
        <table className="multiplication-table">
          <tbody>
            {this.state.table}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default App;