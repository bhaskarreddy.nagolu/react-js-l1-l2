import React from "react";
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      show: true
    };
  }

  componentDidMount() {
    console.log('componentDidMount Called');
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('componentDidUpdate Called');
  }
  
  componentWillReceiveProps(nextProps) {
    console.log('UNSAFE_componentWillReceiveProps called');
  }
  
  componentWillUpdate(nextProps, nextState) {
    console.log('UNSAFE_componentWillUpdate called');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount called');
  }

  toggleShow = () => {
    this.setState({ show: !this.state.show })
  }

  render() {
    return (
      <div onClick={this.toggleShow}>
        <p >{this.state.show ? "Nice thought for the day" : null}</p>
      </div>
    );
  }
}

export default App;