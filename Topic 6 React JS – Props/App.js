import React from "react";
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      
    };
  }

  render() {
    return (
      <React.Fragment>
        {this.props.companyName || 'WIPRO'}
        <br></br>
        {this.props.companyLocation || 'BANGALORE'}
      </React.Fragment>
    );
  }
}

export default App;