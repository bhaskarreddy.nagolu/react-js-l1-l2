
import React from 'react';
import './movie-table.css';
import { Link } from "react-router-dom";
class Table extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            movies: this.props.movies
        };
    }

    componentDidMount() {
        this.setState({ movies: this.props.movies });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.movies !== prevProps.movies) {
            this.setState({ movies: this.props.movies });
        }
    }

    renderHeader = () => {
        const column = this.state.movies.length > 0 && Object.keys(this.state.movies[0]);
        return column && column.map((data) => {
            return <th key={data}>{data.toLocaleUpperCase()}</th>
        })
    }


    handleDelete = (movieId) => {
        let movies = this.state.movies.filter(m => m.movieId !== movieId);
        this.setState({ movies });
    };


    handleView = (movieId) => {
        let movies = this.state.movies.filter(m => m.movieId === movieId);
        this.setState({ movies });
    };

    renderBody = () => {
        const movieList = this.state.movies;
        if (movieList !== undefined) {
            return (
                movieList.map(movies => {
                    var showLink = "/movies/show/" + movies.movieId;
                    var deleteLink = "/movies/delete/" + movies.movieId;
                    return (
                        <tr key={movies.movieId}>
                            <th scope="row"><Link className="show-link" to={showLink}>{movies.movieId}</Link></th>
                            <td><Link className="show-link" to={showLink}>{movies.movieName}</Link></td>
                            <td><Link className="show-link" to={showLink}>{movies.leadActor}</Link></td>
                            <td><Link className="show-link" to={showLink}>{movies.leadActre}</Link></td>
                            <td><Link className="show-link" to={showLink}>{movies.language}</Link></td>
                            <td><Link className="show-link" to={showLink}>{movies.collections}</Link></td>
                            <td><Link className="show-link" to={showLink}>{movies.yearOfRelease}</Link></td>
                            <td><Link to={showLink} onClick={() => this.handleView(movies.movieId)} className="ml-5px btn btn-primary">View</Link></td>
                            <td><Link to={deleteLink} className="ml-5px btn btn-danger" onClick={() => this.handleDelete(movies.movieId)}>Delete</Link></td>
                        </tr>
                    )
                })
            )
        } else {
            return <tr></tr>
        }
    }

    render() {
        return (
            <table className="movies">
                <thead>
                    <tr>{this.renderHeader()}</tr>
                </thead>
                <tbody>
                    {this.renderBody()}
                </tbody>
            </table>
        )
    }
}
export default Table;