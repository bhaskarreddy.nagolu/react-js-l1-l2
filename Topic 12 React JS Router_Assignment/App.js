import React from 'react';
import ListMovieComponent from './components/ListMovieComponent';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
function App() {
  return (
    <div>
      <h2 className="text-center"> Movie Details Management</h2>
      <Router>
        <div className="container">
          <Routes>
            <Route path="/" exact element={<ListMovieComponent />}></Route>
            <Route path="/movies/delete/:movieId" element={<ListMovieComponent />}></Route>
            <Route path="/movies/show/:movieId" element={<ListMovieComponent />}></Route>
            <Route path="/addNewMovie" element={<ListMovieComponent />}></Route>
          </Routes>
        </div>
      </Router>
    </div>
  );
}

export default App;
