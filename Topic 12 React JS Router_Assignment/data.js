const movies = [{
  "movieId": 1,
  "movieName": "RRR",
  "leadActor": "NTR",
  "leadActre": "Shriya",
  "language": "Telugu",
  "collections": "No",
  "yearOfRelease": "2022"
}, {
  "movieId": 2,
  "movieName": "Bahubali",
  "leadActor": "Prabhas",
  "leadActre": "Anushka",
  "language": "Kannada",
  "collections": "Yes",
  "yearOfRelease": "2021"
}, {
  "movieId": 3,
  "movieName": "Avengers: Infinity War Part 2",
  "leadActor": "Ajay",
  "leadActre": "Mitra",
  "language": "English",
  "collections": "No",
  "yearOfRelease": "2020"
}]

export default movies;
