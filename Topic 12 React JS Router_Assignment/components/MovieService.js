import movies from '../data';

export const getMovies = () => {
    return movies;
}

export const addMovie = (movie) => {
    let allMovies = [...movies, movie];
    return allMovies;
}

export const getMovieById = (movieId) => {
    return movies.filter(movie => movie.movieId === movieId);
}

export const deleteMovieById = (movieId) => {
    return movies.filter(movie => movie.movieId !== movieId);
}