import React, { Component } from 'react';
import { getMovies } from './MovieService';
import Table from '../Table';
import { Link } from "react-router-dom";
class ListMovieComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            movies: [],
            movieId: '',
            movieName: "",
            leadActor: "",
            leadActre: "",
            language: "",
            collections: "",
            yearOfRelease: "",
            newMovie: null
        }
    }

    componentDidMount() {
        let movies = getMovies();
        this.setState({ movies: movies });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.movies !== prevState.movies) {
            let movies = getMovies();
            this.setState({ movies: movies });
        }
    }

    handleMovieId = (event) => {
        this.setState({
            movieId: event.target.value
        })
    }

    handleMovieName = (event) => {
        this.setState({
            movieName: event.target.value
        })
    }

    handleELeadActor = (event) => {
        this.setState({
            leadActor: event.target.value
        })
    }

    handleLeadActre = (event) => {
        this.setState({
            leadActre: event.target.value,
        })
    }

    handleLanguage = (event) => {
        this.setState({
            language: event.target.value
        })
    }


    handleCollections = (event) => {
        this.setState({
            collections: event.target.value
        })
    }

    hadleYearOfRelease = (event) => {
        this.setState({
            yearOfRelease: event.target.value
        })
    }

    handleAddMovie = (event) => {
        let newMovie = (<div>
            <h1> Add Movie Details </h1>
            <p>Movie ID (ReadOnly):
                <input type="text" required={false} readOnly value={this.state.movies.length + 1} onChange={this.handleMovieId} />
            </p>
            <p>Movie Name  :
                <input type="text" required={false} onChange={this.handleMovieName} />
            </p>
            <p>Lead Actor :
                <input type="text" onChange={this.handleELeadActor} />
            </p>
            <p>Lead Actre :
                <input type="text" onChange={this.handleLeadActre} />
            </p>
            <p>Language:
                <input type="text" required={false} onChange={this.handleLanguage} />
            </p>
            <p>Collections:
                <input type="text" onChange={this.handleCollections} />
            </p>
            <p>Year Of Release:
                <input type="text" maxLength={4} required={false} onChange={this.hadleYearOfRelease} />
            </p>

            <input type="buttton" value="Add Movie" style={{ "width": "5%", "background": "lightblue" }} onClick={() => this.addMovie(this.state.movies.length + 1)} />

        </div>)
        this.setState({ newMovie: newMovie })
    }

    addMovie = (movieId) => {
        let newMovie = {
            movieId: movieId,
            movieName: this.state.movieName,
            leadActor: this.state.leadActor,
            leadActre: this.state.leadActre,
            language: this.state.language,
            collections: this.state.collections,
            yearOfRelease: this.state.yearOfReleases
        }
        let existingArray = this.state.movies;
        existingArray.push(newMovie)
        this.setState({ movies: existingArray, newMovie : null });
    }

    render() {
        return (
            <React.Fragment>
                <Link to="/addNewMovie" className="ml-5px btn btn-danger" onClick={this.handleAddMovie}><b>Add New Movie</b></Link>
                <Table movies={this.state.movies || []}></Table>
                {this.state.newMovie}
            </React.Fragment>
        )
    }
}

export default ListMovieComponent
