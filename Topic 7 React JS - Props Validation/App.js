import React, { Component } from 'react';  
import PropTypes from 'prop-types';  
class App extends Component {  
   render() {  
      return (  
          <div>  
              <h1>React JS Props validation </h1>  
        </div>  
        );  
   }  
}  

function withinRange(props, propName) {

  if (props[propName]) {
    let value = props[propName];
    if (typeof value === 'number') {
        return (value >= 18 && value <= 60) ? null : new Error(propName + ' is not within 18 to 60');
    }
  }
  // assume all ok
  return null;
}
//Validation
App.propTypes = { 
    name: PropTypes.string, 
    prefferedCities: PropTypes.array,  
    age: withinRange
}  

// Default Props
App.defaultProps = {  
    name: "Steve",  
    prefferedCities: ['Bangalore', 'Chennai'],  
    age: 18, 
}  
export default App;  