import React from 'react';
import ReactDOM from 'react-dom';
//import Main from './components/Main';
import App from './App';
import reportWebVitals from './reportWebVitals';
const cities = ['BNAGALORE', 'CHENNAI','HYDERABAD']
ReactDOM.render(
  <React.StrictMode>
    <App name={"WIPRO"} prefferedCities={cities} age={25}/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
