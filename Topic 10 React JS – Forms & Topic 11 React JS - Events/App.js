import React from "react";
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      physics: '',
      chemistry: '',
      biology: '',
      mathematics: '',
      averageMarks: '',
      error: ''
    };
  }

  handlePhysics = (event) => {
    this.setState({ physics: event.target.value })
  }


  handleChemistry = (event) => {
    this.setState({ chemistry: event.target.value })
  }

  handleBiology = (event) => {
    this.setState({ biology: event.target.value })
  }

  handleMathematics = (event) => {
    this.setState({ mathematics: event.target.value })
  }

  findAverageMarks = (event) => {
    var { physics, chemistry, biology, mathematics } = this.state;
    var physicsM = parseInt(physics);
    var chemistryM = parseInt(chemistry);
    var biologyM = parseInt(biology);
    var mathematicsM = parseInt(mathematics);
    if ((physicsM >= 0 && physicsM <= 100) && (chemistryM >= 0 && chemistryM <= 100) && (biologyM >= 0 && biologyM <= 100) && (mathematicsM >= 0 && mathematicsM <= 100)) {
      var avg = (physicsM + chemistryM + biologyM + mathematicsM) / 4;
      let mesg = "Average Marks : " + avg;
      this.setState({ averageMarks: mesg })
      alert(mesg);
    }
    else {
      this.setState({ error: "Entered Marks is Invalid" })
    }
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.findAverageMarks}>
        <h1>Students Marks Form</h1>
        <p>Physics:
          <input type="number" onChange={this.handlePhysics} />
        </p>
        <p>Chemistry :
          <input type="number" onChange={this.handleChemistry} />
        </p>
        <p>Biology :
          <input type="number" onChange={this.handleBiology} />
        </p>
        <p>Mathematics :
          <input type="number" onChange={this.handleMathematics} />
        </p>
        <p>
          <input type="submit" value="Find Average" />
        </p>
        <b> {this.state.averageMarks}</b>
        <b> {this.state.error}</b>

      </form>
    );
  }
}

export default App;