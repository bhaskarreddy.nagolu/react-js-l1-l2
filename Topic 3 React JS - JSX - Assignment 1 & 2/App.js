import React from "react";
import employees from './employees.json';
import './employees.css';
class App extends React.Component {
  state = {
    employees: employees,
    employeeHeaders: ['Employee ID', 'Employee Name', 'Employee Email ID']
  }

  renderTableHeader() {
    // let header = Object.keys(this.state.employees[0])
    return this.state.employeeHeaders.map((key, index) => {
      return <th key={index}>{key}</th>
    })
  }

  renderTableData() {
    return this.state.employees.map((employee, index) => {
      const { id, name, email } = employee //destructuring
      return (
        <tr key={id + index}>
          <td>{id}</td>
          <td>{name}</td>
          <td>{email}</td>
        </tr>
      )
    })
  }

  render() {
    return (
      <React.Fragment>
        <h3>Employee Details</h3>
        <table className="employees">
          <tbody>
            <tr>{this.renderTableHeader()}</tr>
            {this.renderTableData()}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export default App;